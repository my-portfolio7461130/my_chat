package com.example.rememberme.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Image {
    private Long id;
    private String name;
    private Long size;
    private byte[] bytes;

    public Image(Long id, String name, Long size, byte[] bytes) {
        this.id = id;
        this.name = name;
        this.size = size;
        this.bytes = bytes;
    }
}
