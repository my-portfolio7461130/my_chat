package com.example.rememberme.repository;

import com.example.rememberme.api.MessageStatus;
import com.example.rememberme.model.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@RequiredArgsConstructor
public class MessageRepository {

    private List<Message> messages = new ArrayList<>();

    private final JdbcTemplate jdbcTemplate;

//    public void testDump() {
//        for (int i = 0; i < 100; i++) {
//            Message message = new Message();
//            message.setId((long) i);
//            message.setMessageText("some message test text " + i);
//            message.setIsRead(true);
//            message.setIsDelivered(true);
//            message.setIsDeleted(false);
//            long authorId;
//            if (i % 3 == 0) {
//                authorId = 2L;
//            } else {
//                authorId = 1L;
//            }
//            message.setAuthorId(authorId);
//            message.setTime(new Timestamp(System.currentTimeMillis() - 150_000L - i * 1000));
//            messages.add(message);
//        }
//    }

    public void save(Message message) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        simpleJdbcInsert.withTableName("messages").usingGeneratedKeyColumns("id");

        Map<String, Object> values = new HashMap<>();
        values.put("message_text", message.getMessageText());
        values.put("author_id", message.getAuthorId());
        values.put("time", message.getTime());
        values.put("is_read", message.getIsRead());
        values.put("is_deleted", message.getIsDeleted());
        values.put("is_delivered", message.getIsDelivered());
        values.put("status", message.getStatus());
        values.put("answered_message_id", message.getAnsweredMessageId());
        values.put("image_id", message.getImageId());

        simpleJdbcInsert.execute(values);
    }

    public List<Message> findAll(Integer offset, Integer perPage) {
        String select = "SELECT * FROM messages ORDER BY time DESC LIMIT ? OFFSET ?";
        try {
            return jdbcTemplate.query(select, messageRowMapper, perPage, offset);
        } catch (EmptyResultDataAccessException ex) {
            return new ArrayList<>();
        }
    }

    public Message findById(Long id) {
        String select = "SELECT * FROM messages WHERE id = ?";
        try {
            return jdbcTemplate.query(select, messageRowMapper, id).get(0);
        } catch (EmptyResultDataAccessException | IndexOutOfBoundsException ex) {
            return null;
        }
    }

    public void delete(Message message) {
        String delete = "DELETE FROM messages WHERE id = ?";
        jdbcTemplate.update(delete, message.getId());
        messages.remove(message);
    }

    public void updateAllById(List<Long> messagesToRead) {
        String update = "UPDATE messages SET status = 'READ' WHERE id = ?";

        for (Long messageId : messagesToRead) {
            jdbcTemplate.update(update, messageId);
        }
    }

    RowMapper<Message> messageRowMapper = new RowMapper() {
        @Override
        public Message mapRow(ResultSet rs, int rowNum) throws SQLException {
            Message message = new Message();
            message.setId(rs.getLong("id"));
            message.setMessageText(rs.getString("message_text"));
            message.setAuthorId(rs.getLong("author_id"));
            message.setTime(rs.getTimestamp("time"));
            message.setIsRead(rs.getBoolean("is_read"));
            message.setIsDelivered(rs.getBoolean("is_delivered"));
            message.setIsDeleted(rs.getBoolean("is_deleted"));
            message.setStatus(MessageStatus.valueOf(rs.getString("status")));
            message.setAnsweredMessageId(rs.getLong("answered_message_id"));
            message.setImageId(rs.getLong("image_id"));
            return message;
        }
    };
}
