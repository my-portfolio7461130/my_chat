package com.example.rememberme.repository;

import com.example.rememberme.model.Person;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class PersonRepository {

    private final JdbcTemplate jdbcTemplate;
    List<Person> personList = List.of(
            new Person(1L,"Михаил", "Есипов", "esipov", "123", null, false, null, false),
            new Person(2L,"Альбина", "Булатова", "bulatova", "123", null, false, null, false)
            );

    public Person findByUsername(String username) {

        String select = "SELECT * FROM users WHERE username = ?";
        try {
            return jdbcTemplate.queryForObject(select, personRowMapper, username);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }

    }

    public List<Person> findAll() {
        String select = "SELECT * FROM users";
        try {
            return jdbcTemplate.query(select, personRowMapper);
        } catch (EmptyResultDataAccessException ex) {
            return new ArrayList<>();
        }
    }

    public List<Person> findUsersOnline() {
        String select = "SELECT * FROM users WHERE online_status = true";
        try {
            return jdbcTemplate.query(select, personRowMapper);
        } catch (EmptyResultDataAccessException ex) {
            return new ArrayList<>();
        }
    }

    RowMapper<Person> personRowMapper = new RowMapper<Person>() {
        @Override
        public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
            Person person = new Person();
            person.setId(rs.getLong("id"));
            person.setName(rs.getString("name"));
            person.setFirstName(rs.getString("first_name"));
            person.setLogin(rs.getString("username"));
            person.setPassword(rs.getString("password"));
            person.setOnlineStatus(rs.getBoolean("online_status"));
            person.setAuthToken(rs.getString("auth_token"));
            person.setIsTyping(rs.getBoolean("is_typing"));
            person.setLastOnlineStatusTime(rs.getTimestamp("last_online_status_time"));
            return person;
        }
    };

    public void update(Person person) {
        String update = "UPDATE users SET online_status = ?, last_online_status_time = ? WHERE id = ?";
        jdbcTemplate.update(update, person.getOnlineStatus(), person.getLastOnlineStatusTime(), person.getId());
    }
}
