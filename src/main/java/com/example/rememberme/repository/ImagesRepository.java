package com.example.rememberme.repository;

import com.example.rememberme.model.Image;
import com.mysql.cj.exceptions.DataTruncationException;
import com.mysql.cj.jdbc.exceptions.MysqlDataTruncation;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.DataTruncation;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
@RequiredArgsConstructor
public class ImagesRepository {
    private List<Image> images = new ArrayList<>();

    private final JdbcTemplate jdbcTemplate;

    public Image findById(Long id) {
        String select = "SELECT * FROM images WHERE id = ?";
        try {
            return jdbcTemplate.queryForObject(select, imageRowMapper, id);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    public Long save(Image image) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        simpleJdbcInsert.withTableName("images").usingGeneratedKeyColumns("id");
        Map<String, Object> values = new HashMap<>();
        values.put("name", image.getName());
        values.put("size", image.getSize());
        values.put("bytes", image.getBytes());
        try {
            return simpleJdbcInsert.executeAndReturnKey(values).longValue();
        } catch (DataTruncationException ex) {
            ex.printStackTrace();
            return -1L;
        }
    }

    RowMapper<Image> imageRowMapper = new RowMapper<Image>() {
        @Override
        public Image mapRow(ResultSet rs, int rowNum) throws SQLException {
            Long id = rs.getLong("id");
            String name = rs.getString("name");
            Long size = rs.getLong("size");
            byte[] bytes = rs.getBytes("bytes");
            return new Image(id, name, size, bytes);
        }
    };
}
