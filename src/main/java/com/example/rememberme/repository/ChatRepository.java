package com.example.rememberme.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class ChatRepository {

    private final JdbcTemplate jdbcTemplate;

    public void setTyping(Long id, Boolean isTyping) {
        String update = "UPDATE users SET is_typing = ? WHERE id = ?";
        jdbcTemplate.update(update, isTyping, id);

    }

    public Boolean isTyping(Long id) {
        String select = "SELECT is_typing FROM users WHERE id = ?";
        try {
            return jdbcTemplate.queryForObject(select, Boolean.TYPE, id);
        } catch (EmptyResultDataAccessException ex) {
            return false;
        }
    }
}
